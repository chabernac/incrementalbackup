package chabernac.backup.action;

import java.io.IOException;

import chabernac.backup.resource.IResource;

public class CopyAction implements IAction {
    private final IResource target;

    public CopyAction( IResource aTarget ) {
        super();
        target = aTarget;
    }

    public void execute( IResource aResource ) throws ActionException {
        try {
            aResource.copy( target );
        } catch ( IOException e ) {
            throw new ActionException( "Could not copy resource", e );
        }
    }

    public IAction merge( IAction anAction ) throws ActionException {
        throw new ActionException( "Merge not supported by this action" );
    }

    public IAction isMergedBy( MoveAction aRenameAction ) throws ActionException {
        throw new ActionException( "Merge not supported by this action" );
    }

    public IAction isMergedBy( DeleteAction aDeleteAction ) throws ActionException {
        throw new ActionException( "Merge not supported by this action" );
    }

    public IAction isMergedBy( AddAction aAddAction ) throws ActionException {
        throw new ActionException( "Merge not supported by this action" );
    }
}
