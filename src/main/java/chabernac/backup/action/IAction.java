package chabernac.backup.action;

import chabernac.backup.resource.IResource;

public interface IAction {
    public void execute( IResource aResource ) throws ActionException;

    public IAction merge( IAction anAction ) throws ActionException;

    public IAction isMergedBy( MoveAction aRenameAction ) throws ActionException;

    public IAction isMergedBy( DeleteAction aDeleteAction ) throws ActionException;

    public IAction isMergedBy( AddAction aAddAction ) throws ActionException;

}
