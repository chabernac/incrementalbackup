package chabernac.backup.action;

import chabernac.backup.resource.IResource;

public class AddAction implements IAction {
    private final IResource resource;

    public AddAction( IResource aResource ) {
        super();
        resource = aResource;
    }

    public IResource getResource() {
        return resource;
    }

    public void execute( IResource aResource ) throws ActionException {
    }

    public IAction merge( IAction anAction ) throws ActionException {
        return anAction.isMergedBy( this );
    }

    public IAction isMergedBy( MoveAction aRenameAction ) throws ActionException {
        throw new ActionException( "Merge not suported" );
    }

    public IAction isMergedBy( DeleteAction aDeleteAction ) throws ActionException {
        throw new ActionException( "Merge not suported" );
    }

    public IAction isMergedBy( AddAction aAddAction ) throws ActionException {
        throw new ActionException( "Merge not suported" );
    }

    @Override
    public int hashCode() {
        return resource.getPath().hashCode();
    }

    @Override
    public boolean equals( Object obj ) {
        if ( !( obj instanceof AddAction ) ) return false;

        return ( (AddAction) obj ).getResource().getPath().equals( getResource().getPath() );
    }
}
