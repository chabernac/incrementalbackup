package chabernac.backup.action;

import chabernac.backup.resource.IResource;

public class DeleteAction implements IAction {

    public void execute( IResource aResource ) {
        aResource.delete();
    }

    public IAction merge( IAction anAction ) throws ActionException {
        return anAction.isMergedBy( this );
    }

    public IAction isMergedBy( MoveAction aRenameAction ) {
        return this;
    }

    public IAction isMergedBy( DeleteAction aDeleteAction ) {
        return this;
    }

    public IAction isMergedBy( AddAction aAddAction ) {
        return this;
    }

}
