package chabernac.backup.action;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class ActionList {
    private final Map<String, IAction> actionList = new HashMap<String, IAction>();

    public ActionList addAction( String aHash, IAction aAction ) throws ActionException {
        if ( actionList.containsKey( aHash ) ) {
            actionList.put( aHash, actionList.get( aHash ).merge( aAction ) );
        } else {
            actionList.put( aHash, aAction );
        }
        return this;
    }

    public Map<String, IAction> getActions() {
        return Collections.unmodifiableMap( actionList );
    }

    public void merge( ActionList aList ) throws ActionException {
        for ( Entry<String, IAction> theEntry : aList.getActions().entrySet() ) {
            addAction( theEntry.getKey(), theEntry.getValue() );
        }
    }

    /**
     * compare 2 action lists which only contain add actions
     * 
     * @param aList
     * @return
     * @throws ActionException
     */
    public ActionList diff( ActionList aList ) throws ActionException {
        ActionList theNewActionList = new ActionList();

        // all the entries that exist in the second list but not in the first are added
        for ( Entry<String, IAction> theEntry : aList.getActions().entrySet() ) {
            if ( !actionList.containsKey( theEntry.getKey() ) ) {
                theNewActionList.addAction( theEntry.getKey(), theEntry.getValue() );
            }
        }

        // all the entries that exist in the first list but not in the second are removed
        Map<String, IAction> theOtherActions = aList.getActions();
        for ( Entry<String, IAction> theEntry : getActions().entrySet() ) {
            if ( !theOtherActions.containsKey( theEntry.getKey() ) ) {
                theNewActionList.addAction( theEntry.getKey(), new DeleteAction() );
            }
        }

        for ( Entry<String, IAction> theEntry : getActions().entrySet() ) {
            if ( theOtherActions.containsKey( theEntry.getKey() ) ) {
                AddAction theAction = (AddAction) theEntry.getValue();
                AddAction theOtherAction = (AddAction) theOtherActions.get( theEntry.getKey() );
                if ( !theAction.equals( theOtherAction ) ) {
                    theNewActionList.addAction( theEntry.getKey(), new MoveAction( theOtherAction.getResource().getPath() ) );
                }
            }
        }

        return theNewActionList;
    }
}
