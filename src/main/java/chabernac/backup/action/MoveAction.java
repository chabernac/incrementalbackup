package chabernac.backup.action;

import chabernac.backup.resource.IResource;

public class MoveAction implements IAction {
    private final String newPath;

    public MoveAction( String aNewPath ) {
        super();
        newPath = aNewPath;
    }

    public String getNewPath() {
        return newPath;
    }

    public void execute( IResource aResource ) {
        aResource.move( newPath );
    }

    public IAction merge( IAction anAction ) throws ActionException {
        return anAction.isMergedBy( this );
    }

    public IAction isMergedBy( MoveAction aRenameAction ) {
        return this;
    }

    public IAction isMergedBy( DeleteAction aDeleteAction ) {
        return aDeleteAction;
    }

    public IAction isMergedBy( AddAction aAddAction ) throws ActionException {
        return this;
    }

}
