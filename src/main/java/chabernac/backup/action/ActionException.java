package chabernac.backup.action;

public class ActionException extends Exception {

    private static final long serialVersionUID = 3740678978797640581L;

    public ActionException() {
        super();
    }

    public ActionException( String aMessage, Throwable aCause, boolean aEnableSuppression, boolean aWritableStackTrace ) {
        super( aMessage, aCause, aEnableSuppression, aWritableStackTrace );
    }

    public ActionException( String aMessage, Throwable aCause ) {
        super( aMessage, aCause );
    }

    public ActionException( String aMessage ) {
        super( aMessage );
    }

    public ActionException( Throwable aCause ) {
        super( aCause );
    }
    
    

}
