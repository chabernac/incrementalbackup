/**
 * Copyright (c) 2014 Axa Holding Belgium, SA. All rights reserved.
 * This software is the confidential and proprietary information of the AXA Group.
 */
package chabernac.backup;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

public class FileUtil {
    public static Map<String, FileHash> getFilesForDirectory( String aDirectory, String[] aExtentions ) {
        Map<String, FileHash> theHashes = new HashMap<String, FileHash>();

        Collection<File> theFiles = FileUtils.listFiles(
            new File( aDirectory ),
            aExtentions,
            true );

        for ( File theFile : theFiles ) {
            FileInputStream theFileInpustream = null;
            try {
                theFileInpustream = new FileInputStream( theFile );
                String theHash = DigestUtils.md5Hex( IOUtils.toByteArray( theFileInpustream ) );
                // System.out.println( theFile + "[" + theHash + "]" );
                theHashes.put( theFile.getAbsolutePath().substring( aDirectory.length() ), new FileHash( theFile, theHash ) );
            } catch ( Exception e ) {
                e.printStackTrace();
            } finally {
                if ( theFileInpustream != null ) {
                    try {
                        theFileInpustream.close();
                    } catch ( IOException e ) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return theHashes;
    }

}

