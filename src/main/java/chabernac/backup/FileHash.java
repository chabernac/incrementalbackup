/**
 * Copyright (c) 2013 Axa Holding Belgium, SA. All rights reserved.
 * This software is the confidential and proprietary information of the AXA Group.
 */
package chabernac.backup;

import java.io.File;

public class FileHash {
    private final File   file;
    private final String hash;

    public FileHash( File aFile, String aHash ) {
        super();
        file = aFile;
        hash = aHash;
    }

    public File getFile() {
        return file;
    }

    public String getHash() {
        return hash;
    }

}
