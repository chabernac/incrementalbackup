/**
 * 
 */
/**
 *
 * <br><br>
 * <h3>For all technical information regarding the CORSYCA project please refer to the <a
 * href="http://itpedia.axa.be/confluence/display/APPLMNT/CORSYCA_PORTAL">CORSYCA PORTAL</a></h3>
 * <br>
 * <u><i>Version History</i></u>
 * <br>
 * <br>
 * <pre>
 * v2015.06.0 2 Mar 2015 - DGCH804 - initial release
 *
 * </pre>
 *
 * @version v2015.06.0      2 Mar 2015
 * @author <a href="mailto:guy.chauliac@axa.be"> Guy Chauliac </a>
 */
package chabernac.backup.resource;