package chabernac.backup.resource;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface IResource {
    public void move( String aNewPath );

    public void delete();

    public void copy( IResource aTargetResource ) throws IOException;

    public InputStream getInputStream();

    public OutputStream getOutputStream();

    public String getPath();

}
