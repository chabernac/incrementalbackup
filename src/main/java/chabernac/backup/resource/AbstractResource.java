package chabernac.backup.resource;

import java.io.IOException;

import org.apache.commons.io.IOUtils;

public abstract class AbstractResource implements IResource{

    public void copy( IResource aTargetResource ) throws IOException {
        IOUtils.copy(getInputStream(), aTargetResource.getOutputStream());
    }
    

}
