package chabernac.backup.action;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import chabernac.backup.resource.IResource;

public class ActionListTest {
    @Test
    public void testDiffWithSameLists() throws ActionException {
        ActionList theList = new ActionList()
            .addAction( "a", new AddAction( createDummyResource( "/a/b.txt" ) ) );

        ActionList theList2 = new ActionList()
            .addAction( "a", new AddAction( createDummyResource( "/a/b.txt" ) ) );

        ActionList theResult = theList.diff( theList2 );
        Assert.assertEquals( 0, theResult.getActions().size() );
    }

    @Test
    public void testDiffMovedItemLists() throws ActionException {
        ActionList theList = new ActionList()
            .addAction( "a", new AddAction( createDummyResource( "/a/b.txt" ) ) );

        ActionList theList2 = new ActionList()
            .addAction( "a", new AddAction( createDummyResource( "/a/c.txt" ) ) );

        ActionList theResult = theList.diff( theList2 );
        Assert.assertEquals( 1, theResult.getActions().size() );
        IAction theAction = theResult.getActions().get( "a" );
        Assert.assertTrue( theAction instanceof MoveAction );
        MoveAction theMoveAction = (MoveAction) theAction;
        Assert.assertEquals( "/a/c.txt", theMoveAction.getNewPath() );
    }

    @Test
    public void testDiffDeletedItem() throws ActionException {
        ActionList theList = new ActionList()
            .addAction( "a", new AddAction( createDummyResource( "/a/b.txt" ) ) );

        ActionList theList2 = new ActionList();

        ActionList theResult = theList.diff( theList2 );
        Assert.assertEquals( 1, theResult.getActions().size() );
        IAction theAction = theResult.getActions().get( "a" );
        Assert.assertTrue( theAction instanceof DeleteAction );
    }

    @Test
    public void testDiffAddedItem() throws ActionException {
        ActionList theList = new ActionList();

        ActionList theList2 = new ActionList()
            .addAction( "a", new AddAction( createDummyResource( "/a/b.txt" ) ) );

        ActionList theResult = theList.diff( theList2 );
        Assert.assertEquals( 1, theResult.getActions().size() );
        IAction theAction = theResult.getActions().get( "a" );
        Assert.assertTrue( theAction instanceof AddAction );
    }

    @Test
    public void testMerge() throws ActionException {
        ActionList theList = new ActionList()
            .addAction( "a", new AddAction( createDummyResource( "/a/b.txt" ) ) );

        ActionList theList2 = new ActionList()
            .addAction( "a", new MoveAction( "/a/c.txt" ) );

        ActionList theResult = theList.merge( theList2 );

    }

    private IResource createDummyResource( String aPath ) {
        IResource theResource = Mockito.mock( IResource.class );
        Mockito.when( theResource.getPath() ).thenReturn( aPath );
        return theResource;
    }
}
