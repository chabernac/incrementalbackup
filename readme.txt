the incremental backup will consist of:

- a master: a snapshot of the files at a given time
- increments: the differences between the master and the files at a given time later then the time of the master

e.g.

if backup directory is \backup
it will contain

\backup\master
\backup\2015_02_17
\backup\2015_02_18

each dir will have 

- action file: actions that need to be done to construct the snapshot

For start we will not take duplicate files into account.  So a hash will be unique and can only be linked to a single file

the action file of the master backup directory will only consist of additions

e.g. actions.txt

ADD:FFAABBCC11233:2014\winter\1.jpg 
ADD:FFAABBCC11234:2014\winter\2.jpg

increments will have 

- action file: containing actions that need to be executed on the previous set to come to a snapshot for the time of this increment

actions:

- DELETE: Delete a file with a certain hash: 						e.g.: DELETE:FFAABBCC11234
- MOVE: Move a file with a certain hash to a new location 			e.g.: MOVE:FFAABBCC11233:\2014\zomer\z.jpg

The files which reside in the increment will all be considered as being newly added files.


First backup

- create master backup directory
- copy all files
- create action.txt in backup master directory

First Incremental backup

An index consists of a file path and the file hash code

- obtain action.txt from master backup directory and merge with action files from all increments to obtain an index
- obtain index from source directory
- Compare both index files.  
	- added file detected: copy to increment directory
	- delete detected: write to increment action file
	- move detected: write to increment action file
	
Restore backup

- Merge action files from master and increments to obtain a recovery action scripts.  the recovery action script contain a list of copy commands.

e.g.
ADD:FFAABBCC11233:2014\winter\1.jpg
MOVE:FFAABBCC11233:\2014\zomer\z.jpg

will be merged to

COPY:2014\winter\1.jpg:\2014\zomer\z.jpg

ADD:FFAABBCC11233:2014\winter\1.jpg
DELETE:FFAABBCC11233

will be merged to nothing


Merge increments

only a limited of numbers of increments must be kept. it must be possible to merge the first increment with the master.

- read master action file
- execute increment action file on master and update master action file








